![Neiel Leyva Education](/doc/banner_readme.jpg?raw=true)


# 4-bit Full Adder
An adder is a digital circuit that performs the addition of two binary numbers. A full adder adds control flags to count the values carried. In addition to the two data inputs (A and B), the full adder adds the third input dedicated to the input carry (c-in). In an adder, the normal output is designated as S, which is SUM; the full adder adds a second output for the output carry (c-out).

![1-bit Full Adder](/doc/adder1b.jpg?raw=true)

A full adder logic is designed to scale the number of entries, cascading the carry bit from one adder to another to have an N byte adder; such as a 4-bit adder.

![4-bit Full Adder](/doc/adder4b.jpg?raw=true)


#### Running testbench

In the directory ```/tb```, you can find a testbench ```tb_4bit_full_adder.sv``` and a script to run the testbench ```run_tb.sh```. The script is compatible with Modelsim/Questasim. It is necessary to have the environment variables related to ```vsim``` configured.

To run the testbench with a graphical interface:
```
$ cd tb
$ ./run_tb.sh
```

To run the testbench without a graphical interface, batch mode:
```
$ cd tb
$ ./run_tb.sh -batch
```

#### License

All source code are released under the MIT license. See LICENSE for details.
