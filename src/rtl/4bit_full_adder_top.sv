//   ---------------------------------------------------------- 
//   - File           : 4bit_full_adder_top.sv                
//   - Project        : 4bit_full_adder                   
//   - Organization   : Neiel Leyva - Education           
//   - Author         : Neiel Israel Leyva Santes         
//   - Email          : israel.leyva.santes@gmail.com       
//   - MIT License
//  
//   Copyright (c) 2021 Neiel Leyva - Education
//               
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//               
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//                       
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.   
//   ----------------------------------------------------------

import adder_pkg::*;

module _4bit_full_adder (
    input   bit4_t  dataa_i     ,
    input   bit4_t  datab_i     ,
    input   logic   cin_i       ,
    output  bit4_t  sum_o       ,
    output  logic   cout_o                
);

logic c0;
logic c1;
logic c2;
logic c3;

_1bit_full_adder __adder0__ (
    .a    ( dataa_i[0]  ),
    .b    ( datab_i[0]  ),
    .cin  ( cin_i       ),
    .s    ( sum_o[0]    ),
    .cout ( c0          )              
);

_1bit_full_adder __adder1__ (
    .a    ( dataa_i[1]  ),
    .b    ( datab_i[1]  ),
    .cin  ( c0          ),
    .s    ( sum_o[1]    ),
    .cout ( c2          )              
);

_1bit_full_adder __adder2__ (
    .a    ( dataa_i[2]  ),
    .b    ( datab_i[2]  ),
    .cin  ( c2          ),
    .s    ( sum_o[2]    ),
    .cout ( c3          )              
);

_1bit_full_adder __adder3__ (
    .a    ( dataa_i[3]  ),
    .b    ( datab_i[3]  ),
    .cin  ( c3          ),
    .s    ( sum_o[3]    ),
    .cout ( cout_o      )              
);

endmodule
