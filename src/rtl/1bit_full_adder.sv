//   ---------------------------------------------------------- 
//   - File           : 1bit_full_adder.sv                
//   - Project        : 4bit_full_adder                   
//   - Organization   : Neiel Leyva - Education           
//   - Author         : Neiel Israel Leyva Santes         
//   - Email          : israel.leyva.santes@gmail.com       
//   - MIT License
//  
//   Copyright (c) 2021 Neiel Leyva - Education
//               
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//               
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//                       
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.   
//   ----------------------------------------------------------

module _1bit_full_adder (
    input   logic  a     ,
    input   logic  b     ,
    input   logic  cin   ,
    output  logic  s     ,
    output  logic  cout                
);

logic s1;

assign s1   = a  ^ b                ;
assign s    = s1 ^ cin              ;
assign cout = (a & b) | (s1 & cin)  ; 

endmodule
