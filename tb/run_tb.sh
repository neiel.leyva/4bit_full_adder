#   ---------------------------------------------------------- 
#   - File           : run_tb.sh                
#   - Project        : 4bit_full_adder                   
#   - Organization   : Neiel Leyva - Education           
#   - Author         : Neiel Israel Leyva Santes         
#   - Email          : israel.leyva.santes@gmail.com       
#   - MIT License
#  
#   Copyright (c) 2021 Neiel Leyva - Education
#               
#   Permission is hereby granted, free of charge, to any person obtaining a copy
#   of this software and associated documentation files (the "Software"), to deal
#   in the Software without restriction, including without limitation the rights
#   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#   copies of the Software, and to permit persons to whom the Software is
#   furnished to do so, subject to the following conditions:
#               
#   The above copyright notice and this permission notice shall be included in all
#   copies or substantial portions of the Software.
#                       
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#   SOFTWARE.   
#   ----------------------------------------------------------

#!/bin/bash
#use -batch as a flag to run in batch mode

INCLUDES=./../includes
RTL_DIR=./../src/rtl

CYCLES=-all

rm -rf work

vlog +acc=rn +incdir+ $INCLUDES/*.sv $RTL_DIR/*.sv *.sv 

if [ -z "$1" ] 
then 
    vsim work.tb_4bit_full_adder -do "view wave" -do "run $CYCLES"
else
    vsim work.tb_4bit_full_adder $1 -do "run $CYCLES "
fi
