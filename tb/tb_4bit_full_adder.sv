//   ---------------------------------------------------------- 
//   - File           : tb_4bit_full_adder.sv                
//   - Project        : 4bit_full_adder                   
//   - Organization   : Neiel Leyva - Education           
//   - Author         : Neiel Israel Leyva Santes         
//   - Email          : israel.leyva.santes@gmail.com       
//   - MIT License
//  
//   Copyright (c) 2021 Neiel Leyva - Education
//               
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//               
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//                       
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.   
//   ----------------------------------------------------------

import adder_pkg::*;

module tb_4bit_full_adder;

// input wires
bit4_t  dataa  ;
bit4_t  datab  ;
logic   cin    ;

// output wires
bit4_t  result ;
logic   cout   ;         

// Design under test
_4bit_full_adder __DUT__ (
    .dataa_i  ( dataa  ), 
    .datab_i  ( datab  ), 
    .cin_i    ( cin    ), 
    .sum_o    ( result ), 
    .cout_o   ( cout   )          
);

initial begin
    dataa = 4'h0 ;
    datab = 4'h0 ;
    cin   = 1'h0 ;
    #25
    display_result();

    #25
    dataa = 4'h4 ;
    datab = 4'h5 ;
    cin   = 1'h0 ;
    #25
    display_result();
    
    #25
    dataa = 4'h7 ;
    datab = 4'h2 ;
    cin   = 1'h1 ;
    #25
    display_result();
    
    #25
    dataa = 4'h8 ;
    datab = 4'h7 ;
    cin   = 1'h0 ;
    #25
    display_result();
    
    #25
    dataa = 4'h8 ;
    datab = 4'h7 ;
    cin   = 1'h1 ;
    #25
    display_result();
    
    #25
    $finish;

end

task automatic display_result;
    begin
        $display("Time   : %-0d ns",$time);
        $display("Data A : 0x%h ",dataa ) ;
        $display("Data B : 0x%h ",datab ) ;
        $display("Cin    : 0x%h ",cin   ) ;
        $display("Result : 0x%h ",result) ;
        $display("Cout   : 0x%h ",cout  ) ;
        $display("\n") ;
    end
endtask

endmodule
